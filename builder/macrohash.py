#!/usr/bin/env python
# -*- coding: utf-8 -*-


import hashlib
from pandocfilters import toJSONFilter, Str
import re


def macrohash(key, value, format, meta):
    if key == 'Math':
        if value[0][u't'] == "DisplayMath":
            svgname = '../display/' + hashlib.sha256(re.sub('\n', '',value[1]).encode('utf-8')).hexdigest() + '.svg'
            macro = "DIVDISP include({{" + svgname + "}})ENDDIVDISP"
            return Str(macro)
        if value[0][u't'] == "InlineMath":
            svgname = '../inline/' + hashlib.sha256(re.sub('\n', '',value[1]).encode('utf-8')).hexdigest() + '.svg'
            macro = "include({{" + svgname + "}})"
            return Str(macro)


if __name__ == "__main__":
    toJSONFilter(macrohash)
