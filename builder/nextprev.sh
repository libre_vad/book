#!/bin/bash


set -e
ln=$(cat ../../../markdown/order | grep -n "^$(cat chapt)$" | cut -d":" -f1)
if [[ $1 == 'prev' ]]
then
    if [[ ! $ln = 1 ]]
    then
        lnp=$(echo $ln'-1' | bc)'p'
        lnq=$ln'q'
        echo -e -n "<a href=\""$(sed -n "$lnp;$lnq" ../../../markdown/order)"\">Previous</a>"
    else
        echo "Previous"
    fi
elif [[ $1 == 'next' ]]
then
    if [[ ! $ln = $(wc -l ../../../markdown/order | cut -d' ' -f1) ]]
    then
        lnp=$(echo $ln'+1' | bc)'p'
        lnq=$(echo $ln'+2' | bc)'q'
        echo -e -n "<a href=\""$(sed -n "$lnp;$lnq" ../../../markdown/order)"\">Next</a>"
    else
        echo "Next"
    fi
fi
