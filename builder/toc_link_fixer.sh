#!/bin/bash


set -e
pandoc -f html -t json toc.html | ../../../builder/toc_link_fixer.py > /dev/null
paste links.txt numbers.txt > dictionary
while read -r line
do
    notchap=$(echo $line | cut -d' ' -f2 | grep -E "^[0-9]{1}\.$" > /dev/null; echo $?)
    chpnum=$(echo $line | cut -d' ' -f2 | cut -d'.' -f1)
    p=$chpnum'p'
    q=$(echo $chpnum'+1' | bc)'q'
    chp=$(echo $(sed -n "$p;$q" ../../../markdown/order))
    if [[ $notchap -eq 0 ]]
    then
        echo $chp >> fixed-links
    else
        echo $line | sed -E "s|^.*#{1}||g" | cut -d' ' -f1 | sed -E "s|(^.*$)|"$chp"#\1|g" >> fixed-links
    fi
done < dictionary

paste links.txt fixed-links > fixed-dictionary
while read -r line
do
    sed -i -E "s/\""$(echo $line | cut -d' ' -f1)"\"/\""$(echo $line | cut -d' ' -f2)"\"/g" toc.html
done < fixed-dictionary

rm dictionary fixed-dictionary fixed-links links.txt numbers.txt
