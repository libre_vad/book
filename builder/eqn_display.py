#!/usr/bin/env python

from pandocfilters import toJSONFilter
import re


def extract(key, value, format, meta):

    if key == 'Math':
        if value[0][u't'] == "DisplayMath":
            f = open("equations.txt", "a")
            f.write(re.sub('\n', '',value[1]) + "\n")
            f.close()


if __name__ == "__main__":
    toJSONFilter(extract)
