# Introduction

VAD design is a multidisciplinary field spanning from computer, electrical and
mechanical engineering to different medical and biological disciplines:

* Electrical Engineering
    * Brushless motor design
    * Magnetic bearings

* Mechanical Engineering
    * Centrifugal pump design
    * Fluid mechanics
    * Structural analysis

* Biological Challenges
    * Thrombosis
    * Hemolysis
    * Infection

* Computer science
    * Numerical analysis

* Software
    * OpenSCAD (or ImplicitCAD)
    * FreeCAD (Path workbench)
    * OpenFOAM
    * Salome-Meca
    * KiCad
    * SPICE
    * Camotics
    * LinuxCNC + GMOCCAPY GUI
    * git

One of the most important steps of building a complicated device like a VAD is
simulation. In the next chapter we are going to study the finite element
method (FEM) which is used in magnetic fields or fluid dynamics simulation.
