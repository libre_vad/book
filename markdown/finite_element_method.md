# Finite element method


Finite element method (FEM) is one approach to numerical analysis. The basic
idea of numerical analysis is to discretize and approximate hard to solve
equations. Equations like Navier-Stokes that describes the motion of viscous
fluids or Maxwell's equations of electromagnetism. These discretization
algorithms are the foundation of most simulation programs.