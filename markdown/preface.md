# Preface

Welcome to Libre VAD. We are designing a free/libre/open source [ventricular
assist device](https://en.wikipedia.org/wiki/Ventricular_assist_device) (VAD)
to help patients with a failing heart. According to the World Health
Organization, ischaemic heart disease is the leading cause of death in the
world, claiming more than 9 million lives in 2016. [@t10cofdwho]

A VAD reduces the load on the heart by pumping blood and improving circulation.

It can be used as a bridge to transplantation which means it keeps patients
alive and improves their condition while waiting on the long list of heart
transplants.

In some cases it helps the heart to recover and eliminates the need for the
transplantation. It can also be a destination therapy which means it will
improve and increase the lifespan of patients that are not eligible for a
transplant.


## Disclaimer

Libre VAD is a collaborative and evolving project. It contains errors,
inaccuracies and imperfections. There is absolutely no assurance that any
statement contained or cited in an article touching on medical matters is true,
correct, precise, or up-to-date.  The overwhelming majority of such articles
are written, in part or in whole, by nonprofessionals.  Even if a statement
made about a medical matter is accurate, it may not apply to you or your
symptoms.

The medical information provided on Libre VAD is, at best, of a general nature
and cannot substitute for the advice of a medical professional (for instance, a
qualified doctor/physician, nurse, pharmacist/chemist, and so on).

None of the individual contributors, system operators, developers, sponsors of
Libre VAD nor anyone else connected to Libre VAD can take any responsibility for
the results or consequences of any attempt to use or adopt any of the
information presented on the Libre VAD website or related websites.

Nothing on the Libre VAD website or related websites should be construed as an
attempt to offer or render a medical opinion or otherwise engage in the
practice of medicine.  For more information please take a look at the
[License section](#license).


## How to Contribute

This is a libre/open source project. It is hosted on
[GitLab](https://gitlab.com/libre_vad/book) and any contribution is welcomed
and highly appreciated.

This book is a work in progress and will change frequently.
always check that you have the latest release.

**[TODO]** Add detailed information on how to commit to a git repository for
beginners.

### List of Contributors



## Acknowledgment

The pdf, webpages and ebooks are produced with scripts around
[pandoc](https://pandoc.org/) and other free software.  see the [gitlab
page](https://gitlab.com/libre_vad/book).

Some parts of the [Disclaimer section](#disclaimer) are taken from [wikipedia's
medical disclaimer page
](https://en.wikipedia.org/wiki/Wikipedia:Medical_disclaimer).


## License

Except As Otherwise Noted, This Work Is Licensed Under A Creative Commons
Attribution-ShareAlike 4.0 International License. To View A Copy Of This
License, Visit: [https://creativecommons.org/licenses/by-sa/4.0/
](https://creativecommons.org/licenses/by-sa/4.0/)

Code Samples Are Licensed Under The GNU General Public License v3.0. To View A
Copy Of This License, Visit:
[https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)
