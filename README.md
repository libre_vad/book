# Welcome to Libre VAD

We are designing a free/libre/open source ventricular assist device (VAD) to
help patients with a failing heart.

This book is available on
[https://www.mehranbaghi.com/libre_vad/](https://www.mehranbaghi.com/libre_vad/)
